﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking.Models;
using Microsoft.AspNet.Identity;

namespace Booking.Controllers
{
    [Authorize(Roles = "User")]
    public class CustomerTrainingsController : Controller
    {
        private BookingContext db = new BookingContext();

        // GET: CustomerTrainings
        public ActionResult Index(string sortOrder)
        {
            ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "date_desc" : "";
            ViewBag.NameSortParm = sortOrder == "TrainingType" ? "trainingType_desc" : "TrainingType";
            ViewBag.StartTimeSortParm = sortOrder == "StartTime" ? "startTime_desc" : "StartTime";
            ViewBag.DurationSortParm = sortOrder == "Duration" ? "duration_desc" : "Duration";
            ViewBag.FirstNameSortParm = sortOrder == "FirstName" ? "firstName_desc" : "FirstName";
            ViewBag.MaxParticipantsSortParm = sortOrder == "MaxParticipants" ? "maxParticipants_desc" : "MaxParticipants";
            ViewBag.FreeSpotsSortParm = sortOrder == "FreeSpots" ? "freeSpots_desc" : "FreeSpots";

            var trainings = from s in db.Trainings.Include(t => t.User).Include(t => t.TrainingType).Include("TrainingUsers.User")
                            select s;
            switch (sortOrder)
            {
                case "trainingType_desc":
                    trainings = trainings.OrderByDescending(s => s.TrainingType.Name);
                    break;
                case "TrainingType":
                    trainings = trainings.OrderBy(s => s.TrainingType.Name);
                    break;
                case "startTime_desc":
                    trainings = trainings.OrderByDescending(s => s.StartTime);
                    break;
                case "StartTime":
                    trainings = trainings.OrderBy(s => s.StartTime);
                    break;
                case "duration_desc":
                    trainings = trainings.OrderByDescending(s => s.Duration);
                    break;
                case "Duration":
                    trainings = trainings.OrderBy(s => s.Duration);
                    break;
                case "firstName_desc":
                    trainings = trainings.OrderByDescending(s => s.User.FirstName);
                    break;
                case "FirstName":
                    trainings = trainings.OrderBy(s => s.User.FirstName);
                    break;
                case "maxParticipants_desc":
                    trainings = trainings.OrderByDescending(s => s.TrainingType.MaxParticipants);
                    break;
                case "MaxParticipants":
                    trainings = trainings.OrderBy(s => s.TrainingType.MaxParticipants);
                    break;
                case "freeSpots_desc":
                    trainings = trainings.OrderByDescending(s => s.FreeSpots);
                    break;
                case "FreeSpots":
                    trainings = trainings.OrderBy(s => s.FreeSpots);
                    break;
                case "date_desc":
                    trainings = trainings.OrderByDescending(s => s.Date);
                    break;
                default:
                    trainings = trainings.OrderBy(s => s.Date);
                    break;
            }


            foreach (var item in trainings.ToList())
            {
                int registeredUserCount = item.TrainingUsers.Count();

                int maxSpotsInCurrentTraining = db.TrainingTypes.Where(x => x.Id == item.TrainingTypeId).FirstOrDefault().MaxParticipants;

                item.FreeSpots = maxSpotsInCurrentTraining - registeredUserCount;

                var currentUserInCurrentTrainingCount = item.TrainingUsers.Where(x => x.UserId == User.Identity.GetUserId()).Count();

                if (currentUserInCurrentTrainingCount > 0  )
                {
                    item.CurrentUserJoined = true;
                }
            }
            return View(trainings.ToList().Where(x => x.Date > DateTime.Now));
        }

        // GET: CustomerTrainings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Include("TrainingType").Include("User").Where(x => x.Id == id).FirstOrDefault();

            if (training == null)
            {
                return HttpNotFound();
            }
            return View(training);
        }

        // GET: CustomerTrainings/Join
        public ActionResult Join(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Include("TrainingType").Include("User").Where(x => x.Id == id).FirstOrDefault();
            if (training == null)
            {
                return HttpNotFound();
            }
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);


            ViewBag.CurrentUserFirstName = currentUser.FirstName;
            return View(training);
        }

        // POST: CustomerTrainings/JoinConfirmed
        [HttpPost, ActionName("Join")]
        [ValidateAntiForgeryToken]
        public ActionResult JoinConfirmed(int id)
        {
            TrainingUser trainingUser = new TrainingUser();
            trainingUser.DateAdded = DateTime.Now;
            trainingUser.UserId = User.Identity.GetUserId();
            trainingUser.TrainingId = id;

            db.TrainingUsers.Add(trainingUser);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        // GET: TrainingUsers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Include("TrainingType").Include("User").Where(x => x.Id == id).FirstOrDefault();
            if (training == null)
            {
                return HttpNotFound();
            }
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);

            ViewBag.CurrentUserFirstName = currentUser.FirstName;
            return View(training);

        }

        // POST: TrainingUsers/DeleteConfirmed
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
            TrainingUser trainingUser = db.TrainingUsers.Where(x => x.TrainingId == id && x.UserId == currentUserId).FirstOrDefault();
            db.TrainingUsers.Remove(trainingUser);
            db.SaveChanges();
            return RedirectToAction("Index");

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }            
    }
}
