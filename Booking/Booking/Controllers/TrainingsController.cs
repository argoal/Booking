﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking.Models;

namespace Booking.Controllers
{
    [Authorize(Roles = "Specialist, Admin")]
    public class TrainingsController : Controller
    {
        private Models.BookingContext db = new Models.BookingContext();

        // GET: Trainings
        public ActionResult Index(string sortOrder)
        
        {
            ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "date_desc"  : "";
            ViewBag.TrainingTypeSortParm = sortOrder == "TrainingType" ? "trainingType_desc" : "TrainingType";
            ViewBag.StartTimeSortParm = sortOrder == "StartTime" ? "startTime_desc" : "StartTime";
            ViewBag.DurationSortParm = sortOrder == "Duration" ? "duration_desc" : "Duration";
            ViewBag.FirstNameSortParm = sortOrder == "FirstName" ? "firstName_desc" : "FirstName";
            ViewBag.MaxParticipantsSortParm = sortOrder == "MaxParticipants" ? "maxParticipants_desc" : "MaxParticipants";
            ViewBag.FreeSpotsSortParm = sortOrder == "FreeSpots" ? "freeSpots_desc" : "FreeSpots";

            var trainings = from s in db.Trainings.Include("TrainingType").Include("User")
            select s;

            switch (sortOrder)
            {
                case "trainingType_desc":
                    trainings = trainings.OrderByDescending(s => s.TrainingType.Name);
                    break;
                case "TrainingType":
                    trainings = trainings.OrderBy(s => s.TrainingType.Name);
                    break;
                case "startTime_desc":
                    trainings = trainings.OrderByDescending(s => s.StartTime);
                    break;
                case "StartTime":
                    trainings = trainings.OrderBy(s => s.StartTime);
                    break;
                case "duration_desc":
                    trainings = trainings.OrderByDescending(s => s.Duration);
                    break;
                case "Duration":
                    trainings = trainings.OrderBy(s => s.Duration);
                    break;
                case "firstName_desc":
                    trainings = trainings.OrderByDescending(s => s.User.FirstName);
                    break;
                case "FirstName":
                    trainings = trainings.OrderBy(s => s.User.FirstName);
                    break;
                case "maxParticipants_desc":
                    trainings = trainings.OrderByDescending(s => s.TrainingType.MaxParticipants);
                    break;
                case "MaxParticipants":
                    trainings = trainings.OrderBy(s => s.TrainingType.MaxParticipants);
                    break;
                case "freeSpots_desc":
                    trainings = trainings.OrderByDescending(s => s.FreeSpots);
                    break;
                case "FreeSpots":
                    trainings = trainings.OrderBy(s => s.FreeSpots);
                    break;
                case "date_desc":
                    trainings = trainings.OrderByDescending(s => s.Date);
                    break;
                default:
                    trainings = trainings.OrderBy(s => s.Date);
                    break;
            }
            return View(trainings.ToList().Where(x => x.Date > DateTime.Now));
        }


        // GET: Trainings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
  
            var clientsInCurrentTraining = db.TrainingUsers.Include("User").Where(x => x.TrainingId == id).ToList();

            return View(clientsInCurrentTraining);
        }

        // GET: Trainings/Create
        public ActionResult Create()
        {
            var context = new BookingContext();

            var roleId = context.Roles.Where(x => x.Name == "Specialist").Select(x => x.Id).FirstOrDefault();
            var admins = context.Users.Include(x => x.Roles).Where(x => x.Roles.Any(r => r.RoleId == roleId));

            ViewBag.UserId = new SelectList(admins, "Id", "FirstName");
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name");

            List<int> durations = new List<int>{ 30, 60, 90 };
            ViewBag.Duration = new SelectList(durations);

            return View();
        }

        // POST: Trainings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Date,StartTime,Duration,UserId,TrainingTypeId")] Training training)
        {
            if (ModelState.IsValid)
            {
                ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName", training.UserId);
                ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", training.TrainingTypeId);

                List<int> durations = new List<int> { 30, 60, 90 };
                ViewBag.Duration = new SelectList(durations);

                var newTrainingStartDateWithTime = training.Date.AddHours(training.StartTime.Hour).AddMinutes(training.StartTime.Minute);
                var newTrainingFinishDateWithTime = newTrainingStartDateWithTime.AddMinutes(training.Duration);

                if (newTrainingStartDateWithTime > DateTime.Now)
                {
                    bool timeAlreadyBooked = false;

                    foreach (var oneTraining in db.Trainings)
                    {
                        var savedTrainingStartDateWithTime = oneTraining.Date.AddHours(oneTraining.StartTime.Hour).AddMinutes(oneTraining.StartTime.Minute);
                        var savedTrainingFinishDateWithTime = savedTrainingStartDateWithTime.AddMinutes(oneTraining.Duration);

                        if (newTrainingFinishDateWithTime > savedTrainingStartDateWithTime && newTrainingStartDateWithTime < savedTrainingFinishDateWithTime)
                        {
                            timeAlreadyBooked = true;
                            break;
                        }
                    }

                    if (!timeAlreadyBooked)
                    {
                        db.Trainings.Add(training);
                        db.SaveChanges();
                        TempData["SavedMessage"] = "Treening on lisatud";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Selleks ajaks on planeeritud teine treening. Palun vali uus treeningu kellaaeg.");
                        return View(training);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Treeningu algusaeg peab olema hiljem praegusest kuupäevast ja kellaajast.");
                    return View(training);
                }
            }
            return View(training);
        }

        // GET: Trainings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName", training.UserId);
            //ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", training.TrainingTypeId);


            var context = new BookingContext();

            
            
            var roleId = context.Roles.Where(x => x.Name == "Specialist").Select(x => x.Id).FirstOrDefault();
            var admins = context.Users.Include(x => x.Roles).Where(x => x.Roles.Any(r => r.RoleId == roleId));
            
            ViewBag.UserId = new SelectList(admins, "Id", "FirstName");
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name");

            return View(training);
        }

        // POST: Trainings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,StartTime,Duration,UserId,TrainingTypeId")] Training training)
        {
            if (ModelState.IsValid)
            {

                ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName", training.UserId);
                ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", training.TrainingTypeId);

                var newTrainingStartDateWithTime = training.Date.AddHours(training.StartTime.Hour).AddMinutes(training.StartTime.Minute);
                var newTrainingFinishDateWithTime = newTrainingStartDateWithTime.AddMinutes(training.Duration);

                if (newTrainingStartDateWithTime > DateTime.Now)
                {
                    bool timeAlreadyBooked = false;

                    foreach (var oneTraining in db.Trainings)
                    {
                        var savedTrainingStartDateWithTime = oneTraining.Date.AddHours(oneTraining.StartTime.Hour).AddMinutes(oneTraining.StartTime.Minute);
                        var savedTrainingFinishDateWithTime = savedTrainingStartDateWithTime.AddMinutes(oneTraining.Duration);

                        if (newTrainingFinishDateWithTime > savedTrainingStartDateWithTime && newTrainingStartDateWithTime < savedTrainingFinishDateWithTime)
                        {
                            timeAlreadyBooked = true;
                            break;
                        }
                    }

                    if (!timeAlreadyBooked)
                    {
                        var trainingInDb = db.Trainings.Find(training.Id);
                        trainingInDb.Date = training.Date;
                        trainingInDb.StartTime = training.StartTime;
                        trainingInDb.Duration = training.Duration;
                        trainingInDb.UserId = training.UserId;
                        trainingInDb.TrainingTypeId = training.TrainingTypeId;
                        
                        db.Entry(trainingInDb).State = EntityState.Modified;
                        db.SaveChanges();
                        TempData["SavedMessage"] = "Treeningu muudatused on salvestatud";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Selleks ajaks on planeeritud teine treening. Palun vali uus treeningu kellaaeg.");
                        return View(training);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Treeningu algusaeg peab olema hiljem praegusest kellaajast.");
                    return View(training);
                }
            }
            return View(training);
        }

        // GET: Trainings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Training training = db.Trainings.Include("TrainingType").Include("User").Where(x => x.Id == id).FirstOrDefault();
            if (training == null)
            {
                return HttpNotFound();
            }
            return View(training);
        }

        // POST: Trainings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            Training training = db.Trainings.Include("TrainingType").Include("User").Where(x => x.Id == id).FirstOrDefault();
            db.Trainings.Remove(training);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
