﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Booking.Models;

namespace Booking.Controllers
{
    [Authorize(Roles = "Specialist, Admin")]
    public class ApplicationUsersController : Controller
    {
        private BookingContext db = new BookingContext();

        // GET: ApplicationUsers
        public ActionResult Clients(string id, string sortOrder)
        {
            var clientRoleId = db.Roles.Where(x => x.Name == "User").Select(x => x.Id).FirstOrDefault();
            var clients = from s in db.Users.Include(x => x.Roles).Where(x => x.Roles.Any(r => r.RoleId == clientRoleId))
                      select s;

            ViewBag.FirstNameSortParm = String.IsNullOrEmpty(sortOrder) ? "firstName_desc" : "";
            ViewBag.LastNameSortParm = sortOrder == "LastName" ? "lastName_desc" : "LastName";
            ViewBag.PhoneSortParm = sortOrder == "Phone" ? "phone_desc" : "Phone";
            ViewBag.EmailSortParm = sortOrder == "Email" ? "email_desc" : "Email";

            switch (sortOrder)
            {
                case "firstName_desc":
                    clients = clients.OrderByDescending(s => s.FirstName);
                    break;
                case "LastName":
                    clients = clients.OrderBy(s => s.LastName);
                    break;
                case "lastName_desc":
                    clients = clients.OrderByDescending(s => s.LastName);
                    break;
                case "Phone":
                    clients = clients.OrderBy(s => s.Phone);
                    break;
                case "phone_desc":
                    clients = clients.OrderByDescending(s => s.Phone);
                    break;
                case "Email":
                    clients = clients.OrderBy(s => s.Email);
                    break;
                case "email_desc":
                    clients = clients.OrderByDescending(s => s.Email);
                    break;
                default:
                    clients = clients.OrderBy(s => s.FirstName);
                    break;
            }
            return View(clients);
        }

        public ActionResult AdminsSpecialists(string id, string sortOrder)
        {
            var roleAdminId = db.Roles.Where(x => x.Name == "Admin").Select(x => x.Id).FirstOrDefault();
            var roleSpecialistId = db.Roles.Where(x => x.Name == "Specialist").Select(x => x.Id).FirstOrDefault();

            var users = from s in db.Users.Include(x => x.Roles).Where(x => x.Roles.Any(r => r.RoleId == roleSpecialistId || r.RoleId == roleAdminId))
                          select s;

            ViewBag.FirstNameSortParm = String.IsNullOrEmpty(sortOrder) ? "firstName_desc" : "";
            ViewBag.LastNameSortParm = sortOrder == "LastName" ? "lastName_desc" : "LastName";
            ViewBag.PhoneSortParm = sortOrder == "Phone" ? "phone_desc" : "Phone";
            ViewBag.EmailSortParm = sortOrder == "Email" ? "email_desc" : "Email";
            //ViewBag.RolesSortParm = sortOrder == "Roles" ? "roles_desc" : "Roles";

            switch (sortOrder)
            {
                case "LastName":
                    users = users.OrderBy(s => s.LastName);
                    break;
                case "lastName_desc":
                    users = users.OrderByDescending(s => s.LastName);
                    break;
                case "Phone":
                    users = users.OrderBy(s => s.Phone);
                    break;
                case "phone_desc":
                    users = users.OrderByDescending(s => s.Phone);
                    break;
                case "Email":
                    users = users.OrderBy(s => s.Email);
                    break;
                case "email_desc":
                    users = users.OrderByDescending(s => s.Email);
                    break;
                //case "Roles":
                //    users = users.OrderBy(s => s.Roles);
                //    break;
                //case "roles_desc":
                //    users = users.OrderByDescending(s => s.Roles);
                //    break;
                case "firstName_desc":
                    users = users.OrderByDescending(s => s.FirstName);
                    break;
                default:
                    users = users.OrderBy(s => s.FirstName);
                    break;
            }

            return View(users);
        }



        // GET: ApplicationUsers/Edit/5
        public ActionResult EditClients(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);

            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: ApplicationUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditClients([Bind(Include = "Id,FirstName,LastName,Phone,Email,Roles")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                var applicationUserToSave = db.Users.Find(applicationUser.Id);
                applicationUserToSave.FirstName = applicationUser.FirstName;
                applicationUserToSave.LastName = applicationUser.LastName;
                applicationUserToSave.Phone = applicationUser.Phone;
                applicationUserToSave.Email = applicationUser.Email;

                db.Entry(applicationUserToSave).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Clients");
            }
            return View(applicationUser);
        }



        // GET: ApplicationUsers/Edit/5
        public ActionResult EditAdminsSpecialists(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: ApplicationUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAdminsSpecialists([Bind(Include = "Id,FirstName,LastName,Phone,Email,Roles")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                var applicationUserToSave = db.Users.Find(applicationUser.Id);
                applicationUserToSave.FirstName = applicationUser.FirstName;
                applicationUserToSave.LastName = applicationUser.LastName;
                applicationUserToSave.Phone = applicationUser.Phone;
                applicationUserToSave.Email = applicationUser.Email;

                db.Entry(applicationUserToSave).State = EntityState.Modified;
                //db.Entry(applicationUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AdminsSpecialists");
            }
            return View(applicationUser);
        }


        // GET: ApplicationUsers/DeleteClients/5
        public ActionResult DeleteClients(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: ApplicationUsers/DeleteClients/5
        [HttpPost, ActionName("DeleteClients")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteClientsConfirmed(string id)
        {
            ApplicationUser applicationUser = db.Users.Find(id);
            db.Users.Remove(applicationUser);
            db.SaveChanges();
            return RedirectToAction("Clients");
        }



        // GET: ApplicationUsers/DeleteAdminsSpecialists/5
        public ActionResult DeleteAdminsSpecialists(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        //POST: ApplicationUsers/DeleteAdmins/5
        [HttpPost, ActionName("DeleteAdminsSpecialists")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAdminsSpecialistsConfirmed(string id)
        {
            ApplicationUser applicationUser = db.Users.Find(id);
            db.Users.Remove(applicationUser);
            db.SaveChanges();
            return RedirectToAction("AdminsSpecialists");
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
