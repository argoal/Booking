﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking.Models;

namespace Booking.Controllers
{
    [Authorize(Roles = "Specialist, Admin")]
    public class SpecialistTrainingTypesController : Controller
    {
        private Models.BookingContext db = new Models.BookingContext();

        // GET: SpecialistTrainingTypes
        public ActionResult Index(string sortOrder)
        {
            ViewBag.FirstNameSortParm = String.IsNullOrEmpty(sortOrder) ? "firstName_desc" : "";
            ViewBag.TrainingTypeSortParm = sortOrder == "TrainingType" ? "trainingType_desc" : "TrainingType";

            var specialistTrainingTypes = from s in db.UserTrainingTypes.Include(s => s.User).Include(s => s.TrainingType)
            select s;

            switch (sortOrder)
            {
                case "trainingType_desc":
                    specialistTrainingTypes = specialistTrainingTypes.OrderByDescending(s => s.TrainingType.Name);
                    break;
                case "TrainingType":
                    specialistTrainingTypes = specialistTrainingTypes.OrderBy(s => s.TrainingType.Name);
                    break;
                case "firstName_desc":
                    specialistTrainingTypes = specialistTrainingTypes.OrderByDescending(s => s.User.FirstName);
                    break;
                default:
                    specialistTrainingTypes = specialistTrainingTypes.OrderBy(s => s.User.FirstName);
                    break;
            }
            return View(specialistTrainingTypes.ToList());
        }
    

        // GET: SpecialistTrainingTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTrainingType specialistTrainingType = db.UserTrainingTypes.Find(id);
            if (specialistTrainingType == null)
            {
                return HttpNotFound();
            }
            return View(specialistTrainingType);
        }

        // GET: SpecialistTrainingTypes/Create
        public ActionResult Create()
        {
            var context = new BookingContext();

            var roleId = context.Roles.Where(x => x.Name == "Specialist").Select(x => x.Id).FirstOrDefault();
            var admins = context.Users.Include(x => x.Roles).Where(x => x.Roles.Any(r => r.RoleId == roleId));

            ViewBag.UserId = new SelectList(admins, "Id", "FirstName");
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name");
            return View();
        }

        // POST: SpecialistTrainingTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,TrainingTypeId")] UserTrainingType specialistTrainingType)
        {
            if (ModelState.IsValid)
            {
                db.UserTrainingTypes.Add(specialistTrainingType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SpecialistId = new SelectList(db.Users, "Id", "FirstName", specialistTrainingType.UserId);
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", specialistTrainingType.TrainingTypeId);
            return View(specialistTrainingType);
        }

        // GET: SpecialistTrainingTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTrainingType specialistTrainingType = db.UserTrainingTypes.Find(id);
            if (specialistTrainingType == null)
            {
                return HttpNotFound();
            }
            var context = new BookingContext();

            var roleId = context.Roles.Where(x => x.Name == "Specialist").Select(x => x.Id).FirstOrDefault();
            var admins = context.Users.Include(x => x.Roles).Where(x => x.Roles.Any(r => r.RoleId == roleId));

            ViewBag.UserId = new SelectList(admins, "Id", "FirstName");
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name");
            return View(specialistTrainingType);
        }

        // POST: SpecialistTrainingTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,TrainingTypeId")] UserTrainingType specialistTrainingType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specialistTrainingType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SpecialistId = new SelectList(db.Users, "Id", "FirstName", specialistTrainingType.UserId);
            ViewBag.TrainingTypeId = new SelectList(db.TrainingTypes, "Id", "Name", specialistTrainingType.TrainingTypeId);
            return View(specialistTrainingType);
        }

        // GET: SpecialistTrainingTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTrainingType specialistTrainingType = db.UserTrainingTypes.Include("TrainingType").Include("User").Where(x => x.Id == id).FirstOrDefault();
            //db.UserTrainingTypes.Find(id);
            if (specialistTrainingType == null)
            {
                return HttpNotFound();
            }
            return View(specialistTrainingType);
        }

        // POST: SpecialistTrainingTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserTrainingType specialistTrainingType = db.UserTrainingTypes.Find(id);
            db.UserTrainingTypes.Remove(specialistTrainingType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
