﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Booking.Models
{
    public class TrainingUser
    {
        public int Id { get; set; }
        public DateTime DateAdded { get; set; }


        //Foreign key
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int TrainingId { get; set; }
        public Training Training { get; set; }
        [NotMapped]
        [Display(Name = "Nimi")]
        public string FullName
        {
            get
            {
                if (User != null && User.FirstName != null && User.LastName != null)
                {
                    return String.Format("{0} {1}", User.FirstName, User.LastName);
                }
                else
                {
                    return String.Format("");
                }
            }
        }

    }
}