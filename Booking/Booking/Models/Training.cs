﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Booking.Models
{

    public class Training
    {
        public int Id { get; set; }

        
        [Display(Name = "Kuupäev")]
        [Required(ErrorMessage = "Kuupäev ei või olla tühi.")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        
        [Display(Name = "Kellaaeg")]
        [Required(ErrorMessage = "Kellaaeg ei või olla tühi.")]
        [DataType(DataType.Time)]
        public DateTime StartTime { get; set; }

        
        [Display(Name = "Kestvus")]
        [Required(ErrorMessage = "Kestvus peab olema valitud.")]
        public int Duration
        { get; set; }
        public ICollection<TrainingUser> TrainingUsers { get; set; }

        //Foreign key
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int TrainingTypeId { get; set; }
        public TrainingType TrainingType { get; set; }

        [NotMapped]
        [Display(Name = "Nimi")]
        public string FullName
        {
            get
            {
                if (User != null && User.FirstName != null && User.LastName != null)
                {
                    return String.Format("{0} {1}", User.FirstName, User.LastName);
                }
                else
                {
                    return String.Format("");
                }
            }
        }

        [Display(Name = "Vabu kohti")]
        public int FreeSpots { get; set; }

        [NotMapped]
        public bool CurrentUserJoined { get; set; }
    }
}