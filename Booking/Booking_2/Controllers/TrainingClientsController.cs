﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking.Models;

namespace Booking.Controllers
{
    public class TrainingClientsController : Controller
    {
        private BookingContext db = new BookingContext();

        // GET: TrainingClients
        public ActionResult Index()
        {
            var trainingClients = db.TrainingClients.Include(t => t.Client).Include(t => t.Training);
            return View(trainingClients.ToList());
        }

        //// GET: TrainingClients/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    TrainingClient trainingClient = db.TrainingClients.Find(id);
        //    if (trainingClient == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(trainingClient);
        //}

        // GET: TrainingClients/Create
        public ActionResult Create()
        {
            ViewBag.ClientId = new SelectList(db.Clients, "Id", "Id");
            ViewBag.TrainingId = new SelectList(db.Trainings, "Id", "Id");
            return View();
        }

        // POST: TrainingClients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ClientId,TrainingId")] TrainingClient trainingClient)
        {
            if (ModelState.IsValid)
            {
                trainingClient.DateAdded = DateTime.Now;
                db.TrainingClients.Add(trainingClient);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientId = new SelectList(db.Clients, "Id", "Id", trainingClient.ClientId);
            ViewBag.TrainingId = new SelectList(db.Trainings, "Id", "Id", trainingClient.TrainingId);
            return View(trainingClient);
        }

        // GET: TrainingClients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingClient trainingClient = db.TrainingClients.Find(id);
            if (trainingClient == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientId = new SelectList(db.Clients, "Id", "FirstName", trainingClient.ClientId);
            ViewBag.TrainingId = new SelectList(db.Trainings, "Id", "Id", trainingClient.TrainingId);
            return View(trainingClient);
        }

        // POST: TrainingClients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DateAdded,ClientId,TrainingId")] TrainingClient trainingClient)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trainingClient).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClientId = new SelectList(db.Clients, "Id", "FirstName", trainingClient.ClientId);
            ViewBag.TrainingId = new SelectList(db.Trainings, "Id", "Id", trainingClient.TrainingId);
            return View(trainingClient);
        }

        // GET: TrainingClients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingClient trainingClient = db.TrainingClients.Find(id);
            if (trainingClient == null)
            {
                return HttpNotFound();
            }
            return View(trainingClient);
        }

        // POST: TrainingClients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TrainingClient trainingClient = db.TrainingClients.Find(id);
            db.TrainingClients.Remove(trainingClient);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
